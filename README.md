# scrap-client

To get running with a local development environment as well as a production
build, you need Node.js in Version 10 or higher.

On a Debian or Ubuntu Linux you can follow
https://github.com/nodesource/distributions/blob/master/README.md, if you
want to set up Node.js directly from a repository provided by NodeSource.
More general and different setup instructions can be found on the
[Node.js download page](https://nodejs.org/en/download/).

Once Node is installed, go to the repository root and do:

```
npm install
```

This installs all required libraries. Once this is done, you only need the
following to start up the development server:

```
npm run serve
```

Depending on your setup you might have to change the values in
_.env.development_.

## Deployment

Adapt the values in _.env.production_  and _vue.config.js_  before building
the minified deployment version.

To build the minified version do the following:

```
npm run build
```

Then copy the contents of the _dist_ directory to the web root that you
have configured for the client.
