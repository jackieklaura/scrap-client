import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Scan from '../views/Scan.vue'
import Submit from '../views/Submit.vue'
import ListScans from '../views/ListScans.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/scans',
    name: 'Scans',
    component: ListScans
  },
  {
    path: '/scans/:uuid',
    name: 'Scan',
    component: Scan
  },
  {
    path: '/submit',
    name: 'Submit',
    component: Submit
  },
  {
    path: '/gdpr',
    name: 'GDPR',
    // route level code-splitting
    // this generates a separate chunk (gdpr.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "gdpr" */ '../views/GDPR.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
