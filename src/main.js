import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import VueLogger from 'vuejs-logger'
import VueSimpleMarkdown from 'vue-simple-markdown'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-simple-markdown/dist/vue-simple-markdown.css'

Vue.use(VueSimpleMarkdown)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.config.productionTip = false

Vue.use(VueLogger, {
    isEnabled: true,
    logLevel : ['debug', 'info', 'warn', 'error', 'fatal'].indexOf(process.env.VUE_APP_LOGLEVEL) === -1 ? 'warn' : process.env.VUE_APP_LOGLEVEL,
    stringifyArguments : false,
    showLogLevel : true,
    showMethodName : false,
    separator: '|',
    showConsoleColors: true
})

const app = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

app.$store.$app = app
app.$store.$log = app.$log
