import axios from 'axios'
import util from '../utilities.js'

const state = {
  available: []
}

const getters = {
}

const mutations = {
  clear: (state) => {
    state.available = []
  },
  add: (state, data) => {
    state.available.push(data.scanner)
  }
}

const actions = {
  fetch: (context) => {
    context.commit('clear')
    let uri = process.env.VUE_APP_APIROOT + 'scanners'
    axios.get(uri).then(response => {
      for (let scanner of response.data) {
        context.commit('add', { scanner: scanner })
      }
    }).catch(error => {
      util.handleError(error, 'Could not fetch available scanners from API server')
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
