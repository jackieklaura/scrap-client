import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import scanners from './modules/scanners.js'

Vue.use(Vuex)

const handleError = function (store, error, alertMsg) {
  if (error.response !== undefined) {
    store.$log.error(error.response.status + ' ' + error.response.statusText)
    store.$log.error(error.response)
  } else {
    store.$log.error(error)
  }
  if (alertMsg !== undefined) {
    alert('Error: '+alertMsg+'\nSee console for details.')
  }
}

export default new Vuex.Store({
  strict: true,
  modules: {
    scanners
  },
  state: {
    auth: {
      username: process.env.VUE_APP_STDAPIUSR,
      apikey: process.env.VUE_APP_STDAPIKEY,
    },
    scans: {},
    scanUuids: [],
    issues: [],
    issue: { loading: true },
    files: [],
    explanation: { loading: true },
  },
  mutations: {
    setCredentials (state, data) {
      state.auth.username = data.username
      state.auth.apikey = data.apikey
    },
    setScanUuids (state, uuids) {
      state.scanUuids = uuids
    },
    setScan (state, scan) {
      Vue.set(state.scans, scan.id, {...scan})
    },
    deleteScan (state, uuid) {
      delete state.scans[uuid]
      // this might look a bit hacky, but we have to follow Vue's reactivity rules:
      // https://vuex.vuejs.org/guide/mutations.html#mutations-follow-vue-s-reactivity-rules
      state.scans = {...state.scans}
      const index = state.scanUuids.indexOf(uuid);
      if (index > -1) {
        state.scanUuids.splice(index, 1);
      }
    },
    clearState (state) {
      state.scanUuids = []
      state.scans = {}
    },
    clearIssues (state) {
      state.issues = []
    },
    setIssues (state, issues) {
      state.issues = issues
    },
    clearIssue (state) {
      Vue.set(state, 'issue', { loading: true })
    },
    setIssue (state, issue) {
      Vue.set(state, 'issue', { loading: false, ...issue })
    },
    clearExplanation (state) {
      Vue.set(state, 'explanation', { loading: true })
    },
    setExplanation (state, explanation) {
      Vue.set(state, 'explanation', { loading: false, ...explanation })
    },
  },
  actions: {
    getScans (context) {
      let uri = process.env.VUE_APP_APIROOT + 'scans'
      axios.get(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.commit('setScanUuids', response.data.items)
        // TODO: check the scans array which of the scans are already available
        // and in status 'done', then only load the rest
        for (let scan of response.data.items) {
          context.dispatch({
            type: 'getScan',
            uuid: scan
          })
        }
      }).catch(error => {
        if (!error.response) {
          handleError(this, error, 'could not fetch scans.')
        } else {
          if ([400,401].indexOf(error.response.status) >= 0) {
            let msg = 'The list of scans could not be retrieved for the following reason: \n\n'
            msg += error.response.data.error.message + '\n\n'
            if (error.response.data.error.additionalInfo) {
              msg += 'Additional information provided by the server:\n'
              msg += error.response.data.error.additionalInfo + '\n\n'
            }
            msg += 'Status code: ' + error.response.status + ' ' + error.response.statusText
            alert(msg)
          } else {
            handleError(this, error, 'something weird happened when fetching scans')
          }
        }
      })
    },
    getScan (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'scans/' + data.uuid
      axios.get(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.commit('setScan', response.data)
      }).catch(error => {
        handleError(this, error, 'could not fetch scan.')
      })
    },
    submitScan (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'scans'
      axios.post(uri, data.formData, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.dispatch('getScans')
        this.$app.$router.push({name: 'Scan', params: { uuid: response.data.id }})
      }).catch(error => {
        if (!error.response) {
          handleError(this, error, 'something went wrong when trying to upload a scan')
        } else {
          if ([400,401,413,415].indexOf(error.response.status) >= 0) {
            let msg = 'Your upload was not accepted for the following reason: \n\n'
            msg += error.response.data.error.message + '\n\n'
            if (error.response.data.error.additionalInfo) {
              msg += 'Additional information provided by the server:\n'
              msg += error.response.data.error.additionalInfo + '\n\n'
            }
            msg += 'Status code: ' + error.response.status + ' ' + error.response.statusText
            alert(msg)
          } else {
            handleError(this, error, 'it seems the server did not like our upload')
          }
        }
      })
    },
    deleteScan (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'scans/' + data.uuid
      axios.delete(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(() => {
        context.commit('deleteScan', data.uuid)
      }).catch(error => {
        handleError(this, error, 'could not delete scan.')
      })
    },
    getIssues (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'scans/' + data.scan_uuid + '/issues'
      context.commit('clearIssues')
      context.commit('clearIssue')
      context.commit('clearExplanation')
      axios.get(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.commit('setIssues', response.data.items)
      }).catch(error => {
        handleError(this, error, 'could not fetch issues for scan.')
      })
    },
    getIssue (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'scans/' + data.scan_uuid
      uri += '/issues/' + data.id
      context.commit('clearIssue')
      context.commit('clearExplanation')
      axios.get(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.commit('setIssue', response.data)
        context.dispatch({
          type: 'getExplanation',
          slug: response.data.explanation
        })
      }).catch(error => {
        handleError(this, error, 'could not fetch issue.')
      })
    },
    getExplanation (context, data) {
      let uri = process.env.VUE_APP_APIROOT + 'explanations/' + data.slug
      axios.get(uri, {
        headers: {
          'X-API-USER': context.state.auth.username,
          'X-API-KEY': context.state.auth.apikey,
        }
      }).then(response => {
        context.commit('setExplanation', response.data)
      }).catch(error => {
        if (error.response && error.response.status === 404) {
          context.commit('setExplanation', {notFound: true})
        } else {
          handleError(this, error, 'could not fetch explanation.')
        }
      })
    },
  },
})
