const handleError = function (error, alertMsg) {
  // eslint-disable-next-line no-undef
  let store = app.__vue__.$store
  if (error.response !== undefined) {
    store.$log.error(error.response.status + ' ' + error.response.statusText)
    store.$log.error(error.response)
  } else {
    store.$log.error(error)
  }
  if (alertMsg !== undefined) {
    alert('Error: '+alertMsg+'\nSee console for details.')
  }
}

export default {
  handleError
}
